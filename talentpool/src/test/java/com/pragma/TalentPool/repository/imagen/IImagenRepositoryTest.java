package com.pragma.TalentPool.repository.imagen;

import com.pragma.TalentPool.entity.imagen.DAO.ImagenDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IImagenRepositoryTest {
    @Autowired
    IImagenRepository imagenRepository;

    @Test
    void consultarImagenPorId() {
    }

    @Test
    void findAll() {
        List<ImagenDAO> all = imagenRepository.findAll();
        assertTrue(all.size()==0);

    }

    @Test
    void buscarPorId() {
    }
}