package com.pragma.TalentPool.controller.controlleradvice.exception;

import lombok.Data;

@Data
public class GenericNullPointerException extends NullPointerException{
    private Integer codigo;

    public GenericNullPointerException(String s, Integer codigo) {
        super(s);
        this.codigo = codigo;
    }
}
