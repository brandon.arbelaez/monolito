package com.pragma.TalentPool.repository.persona;

import com.pragma.TalentPool.constantes.ConstanstesTest;
import com.pragma.TalentPool.entity.ciudad.DAO.CiudadDAO;
import com.pragma.TalentPool.entity.persona.DAO.PersonaDAO;
import com.pragma.TalentPool.entity.persona.DTO.PersonaDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

//@DataJpaTest
//@DataMongoTest
@SpringBootTest
class IPersonaRepositoryTest {
    @Autowired
    IPersonaRepository personaRepository;

    /*@Autowired
    IImagenRepository imagenRepository;*/

    @BeforeEach
    void setUp() {

    }

    @Test
    void crearPersona(){
        PersonaDAO personaDAO = PersonaDAO.builder().idPersona(111)
                .personaApellido("Arbelaez")
                .personaEdad(12)
                .personaIdentificacion(123)
                .personaNombre("Brandon")
                .ciudad(ConstanstesTest.CIUDADDAO)
                .tipoDocumento(ConstanstesTest.TIPODOCUMENTODAO).build();
        PersonaDAO save = personaRepository.save(personaDAO);
        assertTrue(save!=null);
    }

    @Test
    void actualizarPersona(){
        PersonaDAO personaDAO = PersonaDAO.builder().idPersona(111)
                .personaApellido("Niño")
                .personaEdad(22)
                .personaIdentificacion(1234638)
                .personaNombre("Brandon")
                .ciudad(ConstanstesTest.CIUDADDAO)
                .tipoDocumento(ConstanstesTest.TIPODOCUMENTODAO).build();
        PersonaDAO save = personaRepository.save(personaDAO);
        assertTrue(save!=null);
    }

    @Test
    void eliminarPersona() {
        Integer integer = personaRepository.eliminarPersona(1);
        assertTrue(integer!=0);
    }

    @Test
    void consultarPersonaPorId() {
        List<PersonaDTO> list = personaRepository.consultarPersonaPorId(1110);
        assertTrue(list.size()>0);
    }

    @Test
    void findById() {
        Optional<PersonaDAO> byId = personaRepository.findById(1);
        assertTrue(!byId.isPresent());
    }

    @Test
    void buscar() {
        List<PersonaDAO> all = personaRepository.findAll();
        assertTrue(all.size()==0);
    }
}