package com.pragma.TalentPool.controller;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.config.Util;
import com.pragma.TalentPool.constantes.ConstanstesTest;
import com.pragma.TalentPool.entity.persona.DAO.PersonaDAO;
import com.pragma.TalentPool.entity.persona.DTO.PersonaDTO;
import com.pragma.TalentPool.repository.imagen.IImagenRepository;
import com.pragma.TalentPool.repository.persona.IPersonaRepository;
import com.pragma.TalentPool.service.IPersonaService;
import com.pragma.TalentPool.service.PersonaServiceImpl;
import org.bson.types.Binary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PersonaControllerImplTest {

    Binary binary;
    MultipartFile multipartFile;
    @Mock
    IPersonaService iPersonaService;
    @InjectMocks
    PersonaControllerImpl personaController;

    @BeforeEach
    void setUp() {
        /*iPersonaService = mock(IPersonaService.class);
        personaController=mock(PersonaControllerImpl.class);*/
    }

    @Test
    void crearPersona() {

    }

    @Test
    void actualizarPersona() {
    }

    @Test
    void consultarPersonas() {
        List<PersonaDTO> listaPersona = Arrays.asList(ConstanstesTest.PERSONADTO);
        when(iPersonaService.consultarPersonas()).thenReturn(listaPersona);
        ResponseEntity<GenericResponseDTO> respuestaTramite = personaController.consultarPersonas();
        assertNotNull(respuestaTramite);
        assertEquals(respuestaTramite.getStatusCode(),HttpStatus.OK);
    }

    @Test
    void consultarPersonaPorId() {
        List<PersonaDTO> listaPersona = Arrays.asList(ConstanstesTest.PERSONADTO);
        when(iPersonaService.consultarPersonaPorId(anyInt())).thenReturn(listaPersona);
        ResponseEntity<GenericResponseDTO> respuestaTramite = personaController.consultarPersonaPorId(anyInt());
        assertNotNull(respuestaTramite);
        assertEquals(respuestaTramite.getStatusCode(),HttpStatus.OK);
    }

    @Test
    void eliminarPersona() {
    }

    @Test
    void respuestaGenerica() {
    }
}