package com.pragma.TalentPool.controller;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.entity.persona.DTO.PersonaDTO;
import com.pragma.TalentPool.service.IPersonaService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("persona")
@CrossOrigin({"*"})
public class PersonaControllerImpl implements IPersonaController {
    private final IPersonaService iPersonaService;
    static GenericResponseDTO GENERIC = new GenericResponseDTO();

    public PersonaControllerImpl(IPersonaService iPersonaService) {
        this.iPersonaService = iPersonaService;
    }

    @Override
    @PostMapping("/")
    @ApiOperation(value = "Crear Persona", notes = "metodo para Crear Persona")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> crearPersona(@ApiParam(type = "json", value = "{\n" +
            "     \"personaNombre\":\"Danilo\",\n" +
            "     \"personaApellido\":\"Arbelaez\",\n" +
            "     \"idTipoDocumento\":1,\n" +
            "     \"personaEdad\":14,\n" +
            "     \"idCiudad\":23,\n" +
            "     \"personaIdentificacion\":123456789\n" +
            " }", example = "{\n" +
            "     \"personaNombre\":\"Danilo\",\n" +
            "     \"personaApellido\":\"Arbelaez\",\n" +
            "     \"idTipoDocumento\":1,\n" +
            "     \"personaEdad\":14,\n" +
            "     \"idCiudad\":23,\n" +
            "     \"personaIdentificacion\":123456789\n" +
            "}") @RequestParam("datos") String json, @RequestParam("image") MultipartFile file) throws ParseException, IOException {
        return iPersonaService.crearPersona(json, file);
    }

    @Override
    @PutMapping("/actualizar")
    @ApiOperation(value = "Crear Persona", notes = "metodo para Crear Persona")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> actualizarPersona(@ApiParam(type = "json", value = "{\n" +
            "     \"idPersona\":1,\n" +
            "     \"personaNombre\":\"Danilo\",\n" +
            "     \"personaApellido\":\"Arbelaez\",\n" +
            "     \"idTipoDocumento\":1,\n" +
            "     \"personaEdad\":14,\n" +
            "     \"idCiudad\":23,\n" +
            "     \"personaIdentificacion\":123456789\n" +
            " }", example = "{\n" +
            "     \"idPersona\":1,\n" +
            "     \"personaNombre\":\"Danilo\",\n" +
            "     \"personaApellido\":\"Arbelaez\",\n" +
            "     \"idTipoDocumento\":1,\n" +
            "     \"personaEdad\":14,\n" +
            "     \"idCiudad\":23,\n" +
            "     \"personaIdentificacion\":123456789\n" +
            "}") @RequestParam("datos") String json, @RequestParam("image") MultipartFile file) throws ParseException {
        return iPersonaService.actualizarPersona(json, file);
    }

    @Override
    @GetMapping("/consultarPersonas")
    @ApiOperation(value = "Listar Personas ", notes = "metodo para listar personas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> consultarPersonas() {
        List<PersonaDTO> respuestaTramite = iPersonaService.consultarPersonas();
        return respuestaGenerica(respuestaTramite);


    }

    @Override
    @GetMapping("/consultarPersona/{id}")
    @ApiOperation(value = "Buscar persona ", notes = "metodo para buscar persona por identificacion")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> consultarPersonaPorId(@ApiParam(type = "Integer", value = "1", example = "1") @PathVariable("id") Integer id) {
        List<PersonaDTO> respuestaTramite = iPersonaService.consultarPersonaPorId(id);
        return respuestaGenerica(respuestaTramite);
    }

    @Override
    @GetMapping("/consultarPersonaPorEdad/{tipoId}/{edadIni}/{edadFin}")
    @ApiOperation(value = "Buscar persona ", notes = "metodo para buscar persona por identificacion")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> consultarPersonaPorEdad(@ApiParam(type = "Integer", value = "1", example = "1") @PathVariable("tipoId")Integer tipoid, @PathVariable("edadIni") Integer rangoEdadInicial, @PathVariable("edadFin") Integer rangoEdadFinal) {
        List<PersonaDTO> respuestaTramite = iPersonaService.consultarPersonaPorEdad(tipoid,rangoEdadInicial,rangoEdadFinal);
        System.out.println(GenericResponseDTO.builder().build().getMessage());
        return respuestaGenerica(respuestaTramite);
    }

    @Override
    @DeleteMapping("/eliminarPersona/{id}")
    @ApiOperation(value = "Eliminar persona", notes = "metodo para eliminar por id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = GenericResponseDTO.class),
            @ApiResponse(code = 400, message = "Bad Request.", response = String.class),
            @ApiResponse(code = 500, message = "Error inesperado del sistema")})
    public ResponseEntity<GenericResponseDTO> eliminarPersona(@ApiParam(type = "Integer", value = "1", example = "1") @PathVariable("id") Integer id) {
        return iPersonaService.eliminarPersona(id);
    }

    public ResponseEntity<GenericResponseDTO> respuestaGenerica(Object respuestaTramite) {
        return new ResponseEntity<>(GenericResponseDTO.builder().message(GenericResponseDTO.builder().build().getMessage())
                .objectResponse(respuestaTramite).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
    }
}
