package com.pragma.TalentPool.entity.persona.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pragma.TalentPool.entity.ciudad.DAO.CiudadDAO;
import com.pragma.TalentPool.entity.imagen.DAO.ImagenDAO;
import com.pragma.TalentPool.entity.persona.DAO.PersonaDAO;
import com.pragma.TalentPool.entity.tipodocumento.DAO.TipoDocumentoDAO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonaDTO {
    //private String _id;
    private Integer idPersona;
    private String personaNombre;
    private String personaApellido;
    private Integer personaEdad;
    private Integer personaIdentificacion;

    /*private String idTipoDocumento;
    private String tipoDocumentoDescripcion;
    private String tipoDocumentoSigla;*/
    private TipoDocumentoDAO tipoDocumento;

    /*private Integer idCiudad;
    private String ciudadDescripcion;
    private Integer ciudadIdDepartamento;*/
    private CiudadDAO ciudad;

    private ImagenDAO imagen;

    public PersonaDTO(PersonaDAO personaDAO) {
        BeanUtils.copyProperties(personaDAO,this);
    }

    public PersonaDTO(PersonaDTO personaDTO) {
        BeanUtils.copyProperties(personaDTO,this);
    }
}
