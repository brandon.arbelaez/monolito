package com.pragma.TalentPool.controller.controlleradvice.exception;

import lombok.Data;

import java.io.IOException;
@Data
public class GenericIoException extends IOException {
    private Integer codigo;

    public GenericIoException(String message, Integer codigo) {
        super(message);
        this.codigo = codigo;
    }
}
