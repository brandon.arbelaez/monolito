package com.pragma.TalentPool.constantes;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.entity.ciudad.DAO.CiudadDAO;
import com.pragma.TalentPool.entity.ciudad.DTO.CiudadDTO;
import com.pragma.TalentPool.entity.imagen.DAO.ImagenDAO;
import com.pragma.TalentPool.entity.imagen.DTO.ImagenDTO;
import com.pragma.TalentPool.entity.persona.DAO.PersonaDAO;
import com.pragma.TalentPool.entity.persona.DTO.PersonaDTO;
import com.pragma.TalentPool.entity.tipodocumento.DAO.TipoDocumentoDAO;
import com.pragma.TalentPool.entity.tipodocumento.DTO.TipoDocumentoDTO;
import com.pragma.TalentPool.repository.imagen.IImagenRepository;
import com.pragma.TalentPool.repository.persona.IPersonaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;

import static org.mockito.Mockito.*;

public class ConstanstesTest {
    public static final ImagenDTO IMAGENDTO = new ImagenDTO();
    public static final CiudadDTO CIUDADDTO = new CiudadDTO();
    public static final TipoDocumentoDTO TIPODOCUMENTODTO = new TipoDocumentoDTO();
    public static final ImagenDAO IMAGENDAO = new ImagenDAO();
    public static final TipoDocumentoDAO TIPODOCUMENTODAO = new TipoDocumentoDAO(1,"Cedula","CC");
    public static final CiudadDAO CIUDADDAO = new CiudadDAO(23,"Ibague",1);
    public static final PersonaDTO PERSONADTO = new PersonaDTO(1,"Brandon","Arbelaez", 1,1234,TIPODOCUMENTODAO,CIUDADDAO,IMAGENDAO);
    public static final PersonaDAO PERSONADAO = new PersonaDAO(12,"Brandon","Arbelaez", TIPODOCUMENTODAO,12,CIUDADDAO,1234);
    public static final GenericResponseDTO GENERIC_RESPONSE_DTO = new GenericResponseDTO("mensaje",PERSONADTO,200);

}
