package com.pragma.TalentPool.service;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.entity.persona.DTO.PersonaDTO;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IPersonaService {
    ResponseEntity<GenericResponseDTO> crearPersona(String json, MultipartFile file) throws ParseException, IOException;
    ResponseEntity<GenericResponseDTO> actualizarPersona(String json, MultipartFile file) throws ParseException;
    List<PersonaDTO> consultarPersonas();
    List<PersonaDTO> consultarPersonaPorId(Integer id);
    List<PersonaDTO> consultarPersonaPorEdad(Integer tipoid, Integer rangoEdadInicial, Integer rangoEdadFinal);
    ResponseEntity<GenericResponseDTO> eliminarPersona(Integer id);
}
