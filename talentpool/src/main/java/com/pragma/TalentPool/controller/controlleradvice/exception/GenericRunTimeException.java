package com.pragma.TalentPool.controller.controlleradvice.exception;

import lombok.Data;

@Data
public class GenericRunTimeException extends RuntimeException {
    private Integer codigo;

    public GenericRunTimeException(String message, Integer codigo) {
        super(message);
        this.codigo = codigo;
    }
}
