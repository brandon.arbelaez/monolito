package com.pragma.TalentPool.controller.controlleradvice;

import com.pragma.TalentPool.controller.controlleradvice.exception.GenericIoException;
import com.pragma.TalentPool.controller.controlleradvice.exception.GenericRunTimeException;
import com.pragma.TalentPool.controller.controlleradvice.exception.GenericNullPointerException;
import com.pragma.TalentPool.entity.error.ErrorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<ErrorDTO> runtimeExceptionHandler(RuntimeException e){
        ErrorDTO errorDTO= ErrorDTO.builder().statusCode(500).mensaje(e.getMessage()).build();
        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST );
    }

    @ExceptionHandler(value = GenericRunTimeException.class)
    public ResponseEntity<ErrorDTO> runTimeExceptionHandler(GenericRunTimeException e){
        ErrorDTO errorDTO= ErrorDTO.builder().statusCode(e.getCodigo()).mensaje(e.getMessage()).build();
        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST );
    }

    @ExceptionHandler(value = GenericNullPointerException.class)
    public ResponseEntity<ErrorDTO> nullPointerExceptionHandler(GenericNullPointerException e){
        ErrorDTO errorDTO= ErrorDTO.builder().statusCode(e.getCodigo()).mensaje(e.getMessage()).build();
        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST );
    }

    @ExceptionHandler(value = GenericIoException.class)
    public ResponseEntity<ErrorDTO> IOExceptionHandler(GenericIoException e){
        ErrorDTO errorDTO= ErrorDTO.builder().statusCode(e.getCodigo()).mensaje(e.getMessage()).build();
        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST );
    }
}
