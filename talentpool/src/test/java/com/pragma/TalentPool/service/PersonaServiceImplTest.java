package com.pragma.TalentPool.service;

import com.pragma.TalentPool.config.GenericResponseDTO;
import com.pragma.TalentPool.config.Util;
import com.pragma.TalentPool.constantes.ConstanstesTest;
import com.pragma.TalentPool.controller.controlleradvice.exception.GenericNullPointerException;
import com.pragma.TalentPool.controller.controlleradvice.exception.GenericRunTimeException;
import com.pragma.TalentPool.entity.imagen.DAO.ImagenDAO;
import com.pragma.TalentPool.entity.persona.DAO.PersonaDAO;
import com.pragma.TalentPool.entity.persona.DTO.PersonaDTO;
import com.pragma.TalentPool.repository.imagen.IImagenRepository;
import com.pragma.TalentPool.repository.persona.IPersonaRepository;
import org.bson.types.Binary;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PersonaServiceImplTest {

    @Mock
    IPersonaRepository repository;
    @Mock
    IImagenRepository imgRepository;
    @Mock
    ModelMapper modelMapper;
    @Mock
    Util util;
    @InjectMocks
    PersonaServiceImpl service;

    Binary binary;
    MultipartFile multipartFile;

    static Integer id;

    @BeforeEach
    void setUp() {
        /*repository =  mock(IPersonaRepository.class);
        imgRepository =mock(IImagenRepository.class);
        modelMapper =mock(ModelMapper.class);
        util=mock(Util.class);
        service = new PersonaServiceImpl(repository, imgRepository, modelMapper,util);*/
        binary = mock(Binary.class);
        ConstanstesTest.IMAGENDAO.set_id("1");
        ConstanstesTest.IMAGENDAO.setDescripcionFoto(binary);
        multipartFile = mock(MultipartFile.class);

    }

    @Test
    void crearPersona() throws ParseException, IOException {
        String json = "";
        //given
        when(util.convertJson(anyString())).thenReturn(ConstanstesTest.PERSONADTO);
        when(repository.buscar(anyInt())).thenReturn(null);
        when(util.fileToBytes(any())).thenReturn(binary);
        when(repository.save(any(PersonaDAO.class))).thenReturn(ConstanstesTest.PERSONADAO);
        when(imgRepository.insert(any(ImagenDAO.class))).thenReturn(ConstanstesTest.IMAGENDAO);

        // when
        ResponseEntity<GenericResponseDTO> response = service.crearPersona(json, multipartFile);

        //then
        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void crearPersonaExcepcion() throws ParseException, IOException {
        String json = "";
        GenericRunTimeException excepcionGeneric = mock(GenericRunTimeException.class);

        when(util.convertJson(anyString())).thenReturn(ConstanstesTest.PERSONADTO);
        when(repository.buscar(anyInt())).thenReturn(ConstanstesTest.PERSONADAO);

        assertThrows(GenericRunTimeException.class, () -> {
            service.crearPersona(json, multipartFile);
        });

    }

    @Test
    void crearPersonaConPersonaExistente() throws ParseException {
        String json = "";
        when(util.convertJson(anyString())).thenReturn(ConstanstesTest.PERSONADTO);
        doThrow(GenericRunTimeException.class).when(repository).buscar(anyInt());

        assertThrows(GenericRunTimeException.class, () -> {
            service.crearPersona(json, multipartFile);
        });
    }

    @Test
    void actualizarPersona() throws ParseException, IOException {
        String json = "";
        List<ImagenDAO> listaImagen = Arrays.asList(ConstanstesTest.IMAGENDAO);
        List<PersonaDAO> listaPersona = Arrays.asList(ConstanstesTest.PERSONADAO);

        //given
        when(util.convertJson(anyString())).thenReturn(ConstanstesTest.PERSONADTO);

        when(repository.findById(any())).thenReturn(Optional.of(ConstanstesTest.PERSONADAO));

        when(repository.save(any(PersonaDAO.class))).thenReturn(ConstanstesTest.PERSONADAO);

        when(imgRepository.listFindByPersonaid(String.valueOf(ConstanstesTest.PERSONADTO.getIdPersona()))).thenReturn(listaImagen);

        when(util.fileToBytes(any())).thenReturn(binary);


        //when
        ResponseEntity<GenericResponseDTO> response = service.actualizarPersona(json, multipartFile);

        //then
        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void actualizarPersonaEntrarAIOException() throws ParseException, IOException {
        String json = "";
        //given
        when(util.convertJson(anyString())).thenReturn(ConstanstesTest.PERSONADTO);

        when(repository.findById(any())).thenReturn(Optional.empty());
        //when
        assertThrows(GenericNullPointerException.class, () -> {
            service.actualizarPersona(json, multipartFile);
        });

    }

    @Test
    void consultarPersonas() {
        List<PersonaDAO> lista = Arrays.asList(ConstanstesTest.PERSONADAO);

        //given
        when(repository.findAll()).thenReturn(lista);

        //when
        List<PersonaDTO> response = service.consultarPersonas();

        //then
        assertNotNull(response);
        response.stream().forEach(x -> {
            id = x.getIdPersona();
        });
        assertEquals(ConstanstesTest.PERSONADAO.getIdPersona(), id);


    }

    @Test
    void consultarPersonaPorIdConPersonaInexistente() throws IOException {
        when(repository.consultarPersonaPorId(anyInt())).thenReturn(Collections.emptyList());

        assertThrows(GenericRunTimeException.class, () -> {
            service.consultarPersonaPorId(anyInt());
        });
    }

    @Test
    void consultarPersonaPorId() throws IOException {
        Binary binary = mock(Binary.class);
        ConstanstesTest.IMAGENDAO.set_id("1");
        ConstanstesTest.IMAGENDAO.setDescripcionFoto(binary);
        List<PersonaDTO> lista = Arrays.asList(ConstanstesTest.PERSONADTO);

        //given
        when(repository.consultarPersonaPorId(anyInt())).thenReturn(lista);

        //when
        List<PersonaDTO> response = service.consultarPersonaPorId(anyInt());

        //then
        response.stream().forEach(x -> {
            id = x.getIdPersona();
        });
        assertEquals(ConstanstesTest.PERSONADTO.getIdPersona(), 1);
        verify(repository).consultarPersonaPorId(anyInt());
    }


    @Test
    void eliminarPersona() {
        //given
        when(repository.eliminarPersona(anyInt())).thenReturn(1);
        when(imgRepository.findByPersonaid(anyString())).thenReturn(ConstanstesTest.IMAGENDAO);

        //when
        ResponseEntity<GenericResponseDTO> response = service.eliminarPersona(anyInt());

        //then
        assertNotNull(response);
        assertEquals(response.getStatusCode(), HttpStatus.OK);

    }

    @Test
    void eliminarPersonaConPersonaInexistente() {
        //given
        when(repository.eliminarPersona(anyInt())).thenReturn(0);
        //when
        assertThrows(GenericRunTimeException.class, () -> {
            service.eliminarPersona(anyInt());
        });
    }
}